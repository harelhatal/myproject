
#include "AI/EnemyAIController.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Characters/CharacterTypes.h"
#include "Characters/FPCharacter.h"
#include "Characters/Enemy/BasicEnemy.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISenseConfig_Sight.h"

AEnemyAIController::AEnemyAIController() {
    PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));

    SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
    SightConfig->SightRadius = 15000.0f;
    SightConfig->LoseSightRadius = 15000.f;
    SightConfig->PeripheralVisionAngleDegrees = 360.0f;
    SightConfig->DetectionByAffiliation.bDetectEnemies = true;
    SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
    SightConfig->DetectionByAffiliation.bDetectFriendlies = true;

    PerceptionComponent->ConfigureSense(*SightConfig);
    PerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());
	    
    StimuliSourceComponent = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("StimuliSource"));
    StimuliSourceComponent->RegisterForSense(TSubclassOf<UAISense_Sight>());
    StimuliSourceComponent->RegisterWithPerceptionSystem();

    AAIController::GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(
        this, &AEnemyAIController::OnTargetDetected);
    AAIController::GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(
        this, &AEnemyAIController::OnPerceptionUpdated);
}

void AEnemyAIController::BeginPlay()
{
    Super::BeginPlay();
    if (BehaviorTree)
    {
        RunBehaviorTree(BehaviorTree);  
    }
}

void AEnemyAIController::OnPossess(APawn* InPawn)
{
    if(BehaviorTree)
    {
        UBlackboardComponent* BlackboardComponent;
        UseBlackboard(BehaviorTree->BlackboardAsset,BlackboardComponent);
        Blackboard = BlackboardComponent;
        RunBehaviorTree(BehaviorTree);
    }
    Super::OnPossess(InPawn);
}

void AEnemyAIController::OnTargetDetected(AActor* Actor, const FAIStimulus Stimulus)
{
    if(BehaviorTree)
    {
        if(Actor->ActorHasTag("Player"))
        {
            ABasicEnemy* MyEnemyCharacter = Cast<ABasicEnemy>(GetPawn());
            if (!MyEnemyCharacter) { return; }
            
            if(Stimulus.WasSuccessfullySensed())
            {
                GetBlackboardComponent()->SetValueAsObject("Hostile",Actor);
                GetBlackboardComponent()->SetValueAsVector("PlayerLocation",Actor->GetActorLocation());
                MyEnemyCharacter->SetEnemyState(EEnemyState::EES_WaitToAttack);
            }
            else
            {
                GetBlackboardComponent()->SetValueAsObject("Hostile",nullptr);
                GetBlackboardComponent()->SetValueAsVector("PlayerLocation",FVector::ZeroVector);
                MyEnemyCharacter->SetEnemyState(EEnemyState::EES_Idle);
            }
        }
    }
}

void AEnemyAIController::OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
    for ([[maybe_unused]] AActor* Actor : UpdatedActors)
    {
    }
}

