#include "Gamemodes/CustomGamemode.h"

#include "Characters/FPCharacter.h"
#include "GameFramework/GameMode.h"
#include "Interfaces/TargetableInterface.h"
#include "Kismet/GameplayStatics.h"

void ACustomGamemode::BeginPlay()
{
	Super::BeginPlay();
	CurrentLevelName = *UGameplayStatics::GetCurrentLevelName(this, true);
	InitializeLevel();
}

int ACustomGamemode::GetCurrentLevelMaxShot()
{
	return MaxShotLevelMap[CurrentLevelName];
}

void ACustomGamemode::RestartLevel()
{
	OpenLevel(CurrentLevelName);
}

void ACustomGamemode::InitializeLevel()
{
	TArray<AActor*> Targets;
	UGameplayStatics::GetAllActorsWithInterface(GetWorld(), UTargetableInterface::StaticClass(), Targets);

	for (AActor* ActorTarget : Targets)
	{
		ITargetableInterface* Target = Cast<ITargetableInterface>(ActorTarget);
		Target->OnTargetDestroyed.AddDynamic(this, &ACustomGamemode::OnTargetDestroyed);
		TargetAmount++;
	}

	TArray<AActor*> PlayersActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFPCharacter::StaticClass(), PlayersActors);
	if (PlayersActors[0])
	{
		AFPCharacter* Player = Cast<AFPCharacter>(PlayersActors[0]);
		if (Player)
		{
			Player->OnPlayerDies.AddDynamic(this, &ACustomGamemode::RestartLevel);
		}
	}
}

void ACustomGamemode::OpenLevel(FName LevelName)
{
	UWorld* World = GetWorld();
	if (World)
	{
		UGameplayStatics::OpenLevel(World, LevelName, false);
		InitializeLevel();
	}
}

void ACustomGamemode::OnLevelSucceeded()
{
	const FName NextLevelName = NextLevelMap[CurrentLevelName];
	OpenLevel(NextLevelName);
	CurrentLevelName = NextLevelName;
}

void ACustomGamemode::CheckShotCount(const int ShotCount)
{
	if (ShotCount >= MaxShotLevelMap[CurrentLevelName])
	{
		RestartLevel();
	}
}


void ACustomGamemode::OnTargetDestroyed()
{
	TargetAmount--;
	if (TargetAmount == 0)
	{
		OnLevelSucceeded();
	}
}
