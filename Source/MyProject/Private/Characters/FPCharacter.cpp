#include "Characters/FPCharacter.h"

#include "Blueprint/UserWidget.h"
#include "Characters/CharacterTypes.h"
#include "Components/AttributesComponent.h"
#include "Gamemodes/CustomGamemode.h"
#include "Items/Weapons/Weapon.h"
#include "Kismet/GameplayStatics.h"
#include "UI/HealthWidget.h"
#include "UI/LevelsWidget.h"


void AFPCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (WeaponClass != nullptr)
	{
		if (AWeapon* WeaponSpawn = GetWorld()->SpawnActor<AWeapon>
				(WeaponClass, FVector::ZeroVector, FRotator::ZeroRotator);
			WeaponSpawn != nullptr)
		{
			WeaponSpawn->Equip(this, TEXT("WeaponSocket"));
		}
	}
	BreathState = EBreathState::EBS_Breath;
	CameraState = ECameraState::ECS_Mesh;
	this->Tags.Add(TEXT("Player"));

	APlayerController* PlayerController = Cast<APlayerController>(GetController());

	if (HoldCrosshairWidgetClass && PlayerController)
	{
		HoldCrosshairWidget = CreateWidget<UUserWidget>(PlayerController, HoldCrosshairWidgetClass);
		if (HoldCrosshairWidget)
		{
			HoldCrosshairWidget->AddToViewport();
			HoldCrosshairWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	if (HealthWidgetClass && PlayerController)
	{
		HealthWidget = CreateWidget<UHealthWidget>(PlayerController, HealthWidgetClass);
		if (HealthWidget)
		{
			HealthWidget->AddToViewport();
			HealthWidget->SetVisibility(ESlateVisibility::Visible);
			HealthWidget->CollapseAll();
		}
	}
	
	AGameModeBase* GameModeBase = UGameplayStatics::GetGameMode(GetWorld());
	Gamemode = Cast<ACustomGamemode>(GameModeBase);


	if (LevelsWidgetClass && PlayerController)
	{
		LevelsWidget = CreateWidget<ULevelsWidget>(PlayerController, LevelsWidgetClass);
		if (HealthWidget)
		{
			LevelsWidget->AddToViewport();
			LevelsWidget->SetVisibility(ESlateVisibility::Visible);
			LevelsWidget->SetBulletTextBlock(Gamemode->GetCurrentLevelMaxShot());
			const FString NumOfLevel = TCHAR_TO_UTF8(*Gamemode->CurrentLevelName.ToString().Right(1));
			LevelsWidget->SetLevelTextBlock(NumOfLevel);
		}
	}
}

void AFPCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AFPCharacter::ReceiveDamage(float Damage)
{
	Super::ReceiveDamage(Damage);
	HealthWidget->UpdateHealthDisplay(Attributes->GetHealthPercent());
	
	if (!Attributes->IsAlive())
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, "Die");
		OnPlayerDies.Broadcast();
	}
	
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Health percent: %f"), Attributes->GetHealthPercent() * 100));
}

void AFPCharacter::Walk(const FInputActionValue& Value)
{
	if (BreathState == EBreathState::EBS_Breath)
	{
		Super::Walk(Value);
	}
}

float AFPCharacter::GetRightLeftAngle()
{
	FVector Velocity = GetVelocity();
	Velocity.Z = 0;

	FVector ForwardVector = GetActorForwardVector();
	FVector RightVector = GetActorRightVector();

	Velocity.Normalize();
	ForwardVector.Normalize();
	RightVector.Normalize();

	float RightDotVelocity = FVector::DotProduct(RightVector, Velocity);
	float ForwardDotVelocity = FVector::DotProduct(ForwardVector, Velocity);

	float Angle = FMath::Atan2(RightDotVelocity, ForwardDotVelocity) * FMath::RadiansToDegrees(1.0f);

	if (ForwardDotVelocity < 0) // Moving backwards
	{
		if (RightDotVelocity > 0) // and to the right
		{
			Angle = 180 - Angle; // Moving back and right should increase the angle
		}
		else if (RightDotVelocity < 0) // and to the left
		{
			Angle = -180 - Angle; // Moving back and left should decrease the angle
		}
	}

	if (Angle > 90)
	{
		Angle -= 180;
	}
	else if (Angle < -90)
	{
		Angle += 180;
	}

	return Angle;
}

void AFPCharacter::SetNewViewTarget(AActor* NewTarget)
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (PlayerController != nullptr)
	{
		PlayerController->SetViewTargetWithBlend(NewTarget, 0.15f, EViewTargetBlendFunction::VTBlend_Linear);
	}
}

void AFPCharacter::OnShot(float NewCountShot)
{
	PlayAttackMontage();
	CountShot = NewCountShot;
	
	if (Gamemode)
	{
		LevelsWidget->SetBulletTextBlock(Gamemode->GetCurrentLevelMaxShot() - CountShot);

		const FString NumOfLevel = TCHAR_TO_UTF8(*Gamemode->CurrentLevelName.ToString().Right(1));
		LevelsWidget->SetLevelTextBlock(NumOfLevel);
		
		Gamemode->CheckShotCount(CountShot);
	}
	
	// TODO: update hud and level if needed
}

AFPCharacter::AFPCharacter()
{
}

void AFPCharacter::ShowHoldCrosshair()
{
	if (HoldCrosshairWidget)
	{
		HoldCrosshairWidget->SetVisibility(ESlateVisibility::Visible);
	}
}

void AFPCharacter::HideHoldCrosshair()
{
	if (HoldCrosshairWidget)
	{
		HoldCrosshairWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}