#include "Characters/Enemy/BasicEnemy.h"

#include "Animations/CustomAnimInstance.h"
#include "Components/AIBehavior.h"
#include "Components/AttributesComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Interfaces/HitInterface.h"
#include "Items/Weapons/Weapon.h"

class AWeapon;

ABasicEnemy::ABasicEnemy()
{
	Behavior = CreateDefaultSubobject<UAIBehavior>(TEXT("Behavior"));
}

void ABasicEnemy::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (UCustomAnimInstance* CustomAnim = Cast<UCustomAnimInstance>(AnimInstance))
	{
		const FVector Velocity = GetCharacterMovement()->Velocity;
		const float Speed = Velocity.Size();
		CustomAnim->SetSpeed(Speed);
	}
}

void ABasicEnemy::Attack(FVector AttackLocation) const
{
	PlayAttackMontage();
	WeaponSpawn->AttackEnemy(AttackLocation);
}

void ABasicEnemy::PlayAttackMontage() const
{
	PlayMontageSection(AttackMontage, "Fire");
	Super::PlayAttackMontage();
}

void ABasicEnemy::ChangeRotation(FRotator NewRotation) const
{
	RootComponent->SetWorldRotation(NewRotation);
}

void ABasicEnemy::BeginPlay()
{
	Super::BeginPlay();
	if (WeaponClass != nullptr)
	{
		if (WeaponSpawn =GetWorld()->SpawnActor<AWeapon>
		(WeaponClass, FVector::ZeroVector, FRotator::ZeroRotator);
		WeaponSpawn != nullptr)
		{
			WeaponSpawn->AttachMeshToSocket(GetMesh(), TEXT("WeaponSocket"));
		}
	}
}

void ABasicEnemy::ReceiveDamage(float Damage)
{
	Attributes->ReceiveDamage(Damage);
	
	if (!Attributes->IsAlive() && IsKillable)
	{
		Destroy();
		WeaponSpawn->Destroy();
	}
}

