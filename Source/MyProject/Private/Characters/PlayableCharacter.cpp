#include "Characters/PlayableCharacter.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Characters/CharacterTypes.h"
#include "Components/AttributesComponent.h"
#include "Components/CapsuleComponent.h"
#include "Items/Weapons/Weapon.h"
#include "Kismet/GameplayStatics.h"

class UEnhancedInputLocalPlayerSubsystem;

APlayableCharacter::APlayableCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = true;
	bUseControllerRotationPitch = true;

	CharacterState = ECharacterState::ECS_Equipped;
	
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	
	CameraView = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraView->SetupAttachment(GetCapsuleComponent());
	CameraView->bUsePawnControlRotation = true;
	CameraRotationSpeed = 150.0f;

	GetMesh()->SetupAttachment(CameraView);

	FullBodyMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandsMesh"));
	FullBodyMesh->SetupAttachment(CameraView);
	FullBodyMesh->bCastDynamicShadow = false;
	FullBodyMesh->CastShadow = false;
	
}

void APlayableCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (const APlayerController* PlayerController = Cast<APlayerController>(GetController());
		Controller && PlayerController && PlayerController->GetLocalPlayer())
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem =
			ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(MappingContext, 1);
		}
	}
	
	CharacterState = ECharacterState::ECS_Equipped;

}

void APlayableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &APlayableCharacter::Walk);
		EnhancedInputComponent->BindAction(MoveCameraAction, ETriggerEvent::Triggered, this, &APlayableCharacter::MoveCamera);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &APlayableCharacter::JumpStart);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Canceled, this, &APlayableCharacter::JumpStop);
	}
}


void APlayableCharacter::Walk(const FInputActionValue& Value)
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	APlayerCameraManager* CameraManager = PlayerController->PlayerCameraManager;
	
	float MovementSpeed;
	const FVector Direction = Value.Get<FVector>();
	FVector CharacterForward = CameraManager->GetActorForwardVector();
	CharacterForward.Z = 0;
	
	const FVector CharacterRight = GetActorRightVector();

	FVector AdjustedDirection = (CharacterForward * Direction.Y) + (CharacterRight * Direction.X);
	AdjustedDirection = AdjustedDirection.GetSafeNormal();

	if (CharacterState == ECharacterState::ECS_HoldAim)
	{
		MovementSpeed = WalkingSpeedHoldAim;
	}
	else
	{
		MovementSpeed = WalkingSpeed;
	}

	AddMovementInput(AdjustedDirection, MovementSpeed);

	if (Direction.X != 0) // If there is lateral movement
	{
		float RotationAmount = Direction.X * 5.0f; // Adjust this value to control the effect intensity
		FRotator NewRotation = GetActorRotation();
		NewRotation.Yaw += RotationAmount;
		SetActorRotation(NewRotation);
	}
}

void APlayableCharacter::MoveCamera(const FInputActionValue& Value)
{
	const FVector AxisValue = Value.Get<FVector>();

	AddControllerYawInput(AxisValue.X * CameraRotationSpeed * GetWorld()->GetDeltaSeconds() * 0.4);
	AddControllerPitchInput(-AxisValue.Y * CameraRotationSpeed * GetWorld()->GetDeltaSeconds() * 0.4);
}

void APlayableCharacter::ReceiveDamage(float Damage)
{
	IHitInterface::ReceiveDamage(Damage);
	Attributes->ReceiveDamage(Damage);

}


void APlayableCharacter::PlayAttackMontage() const
{
}
