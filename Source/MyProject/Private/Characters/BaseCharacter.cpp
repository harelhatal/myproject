#include "Characters/BaseCharacter.h"

#include "Characters/CharacterTypes.h"
#include "Components/AttributesComponent.h"

ABaseCharacter::ABaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	Attributes = CreateDefaultSubobject<UAttributesComponent>(TEXT("Attributes"));
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseCharacter::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseCharacter::PlayHitReactMontage(const FName& SectionName)
{
}

void ABaseCharacter::PlayAttackMontage() const
{
}

void ABaseCharacter::PlayDeathMontage()
{
	
}

void ABaseCharacter::PlayMontageSection(UAnimMontage* Montage, const FName& SectionName) const
{
	if (!Montage)
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Montage is null!"));
		return;
	}

	if (UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
	{
		if (!AnimInstance)
		{
			if(GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("AnimInstance is null!"));
			return;
		}
		
		if (Montage->IsValidSectionName(SectionName))
		{
			AnimInstance->Montage_Play(Montage);
			AnimInstance->Montage_JumpToSection(SectionName, Montage);
		}
		else
		{
			if(GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Invalid section name!"));
		}
	}
	else
	{
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("GetMesh() or GetAnimInstance() returned null!"));
	}
}
