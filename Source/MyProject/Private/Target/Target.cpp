#include "Target/Target.h"

#include "Components/AttributesComponent.h"
#include "Kismet/GameplayStatics.h"

ATarget::ATarget()
{
	bIsSpatiallyLoaded = false;
	PrimaryActorTick.bCanEverTick = true;
	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>("ItemMesh");
	RootComponent = ItemMesh;
	Attributes = CreateDefaultSubobject<UAttributesComponent>(TEXT("AttributesComponent"));
}


void ATarget::CheckHealthAndAct()
{
	if (!Attributes->IsAlive())
	{
		UWorld* World = GetWorld();
		
		OnTargetDestroyedHandler();
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);
		
		if (World)
		{
			UParticleSystemComponent* MuzzleEffectComponent = UGameplayStatics::SpawnEmitterAtLocation(
				World,
				DieEffect,
				ItemMesh->GetComponentLocation() + FVector(0,0,10.f),
				ItemMesh->GetComponentRotation(),
				FVector(1.f, 1.f, 1.f),
				true
			);
		}
	}
}

void ATarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATarget::ReceiveDamage(const float Damage)
{
	Attributes->ReceiveDamage(Damage);
	CheckHealthAndAct();
}

bool ATarget::IsAlive() const
{
	return Attributes->IsAlive();
}

