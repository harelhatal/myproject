#include "Target/TargetConveyor.h"

#include "Target/Target.h"

ATargetConveyor::ATargetConveyor()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ATargetConveyor::BeginPlay()
{
	Super::BeginPlay();
	InitializeTargets();
	FVector Direction = (EndLocation - StartLocation).GetSafeNormal();
	FVector LineEnd = StartLocation + Direction * (DistanceBetweenTargets * (NumberOfTargets - 1));
	//DrawDebugLine(GetWorld(), StartLocation, LineEnd, FColor::Green, true, -1, 0, 6);

}

void ATargetConveyor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!Targets.IsEmpty())
	{
		MoveTargets(DeltaTime);
	}
}

void ATargetConveyor::InitializeTargets()
{
	StartLocation = GetActorLocation(); // Or specify your start location
	FVector Direction = GetActorRotation().Vector(); // Direction for target alignment

	for(int i = 0; i < NumberOfTargets; i++)
	{
		FVector TargetLocation;
		if (Left)
		{
			TargetLocation = StartLocation + (Direction * DistanceBetweenTargets * i);

		} else
		{
			TargetLocation = StartLocation - (Direction * DistanceBetweenTargets * i);

		}
		//TargetLocation = StartLocation + (Direction * DistanceBetweenTargets * i);
		ATarget* Target = GetWorld()->SpawnActor<ATarget>(TargetClass, TargetLocation, FRotator::ZeroRotator);
		if(Target)
		{
			Targets.Add(Target);
		}
	}
	EndLocation = StartLocation + (Direction * DistanceBetweenTargets * (NumberOfTargets - 1)); // Calculate the end location
}

void ATargetConveyor::MoveTargets(float DeltaTime)
{
	FVector Direction = (EndLocation - StartLocation).GetSafeNormal();
	if (!Left)
	{
		Direction = -Direction;
	}

	float LoopDistance = (EndLocation - StartLocation).Size() + AddedDistance; // Total loop distance including added distance
	
	for(ATarget* Target : Targets)
	{
		FVector CurrentLocation = Target->GetActorLocation();
		FVector MoveDirection = Direction;
		FVector NewLocation = CurrentLocation + (MoveDirection * MovementSpeed * DeltaTime);

		float DistanceFromStart = (NewLocation - StartLocation).Size();

		if(DistanceFromStart >= LoopDistance)
		{
			float Overshoot = DistanceFromStart - LoopDistance;
			FVector AdjustedStartLocation = StartLocation + (Direction * Overshoot); // Adjust start based on overshoot
			Target->SetActorLocation(AdjustedStartLocation);
		}
		else
		{
			Target->SetActorLocation(NewLocation);
		}
	}
}

