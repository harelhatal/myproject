#include "Animations/EnemyAnimInstance.h"

#include "Characters/Enemy/BasicEnemy.h"

void UEnemyAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	OwningEnemy = Cast<ABasicEnemy>(TryGetPawnOwner());
	if (!OwningEnemy)
	{
		return ;
	}
	SetEnemyState(OwningEnemy->GetEnemyState());
	
	Super::NativeUpdateAnimation(DeltaSeconds);
}
