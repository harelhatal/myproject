#include "Animations/CustomAnimInstance.h"

#include "Characters/FPCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

void UCustomAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	OwningCharacter = Cast<AFPCharacter>(TryGetPawnOwner());
	if (!OwningCharacter)
	{
		return ;
	}

	SetSpeed(OwningCharacter->GetVelocity().Size());
	IsJumping = OwningCharacter->GetCharacterMovement()->IsFalling();
	SetCharacterState(OwningCharacter->GetCharacterState());
	RightLeftAngle = OwningCharacter->GetRightLeftAngle();
	BreathState = OwningCharacter->GetBreathState();
}
