// Fill out your copyright notice in the Description page of Project Settings.


#include "Tasks/WaitAndAImTask.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Characters/Enemy/BasicEnemy.h"

class ABasicEnemy;

UWaitAndAImTask::UWaitAndAImTask()
{
	bNotifyTick = 1;
}

EBTNodeResult::Type UWaitAndAImTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// Get AI Controller and Pawn

	// Start timer for 4 seconds to end task
	FTimerHandle TimerHandle;
	OwnerComp.GetWorld()->GetTimerManager().SetTimer(TimerHandle, [this, &OwnerComp]()
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}, WaitingTime, false);



	return EBTNodeResult::InProgress; 
}

void UWaitAndAImTask::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIController* AIController = OwnerComp.GetAIOwner();
	ABasicEnemy* EnemyCharacter = Cast<ABasicEnemy>(AIController->GetPawn());
	if (!EnemyCharacter) { return; }

	UObject* HostileObject = OwnerComp.GetBlackboardComponent()->GetValueAsObject("Hostile");
	AActor* HostileActor = Cast<AActor>(HostileObject);
	if (!HostileActor) return;

	FVector Direction = HostileActor->GetActorLocation() - EnemyCharacter->GetActorLocation();
	FRotator TargetRotation = FRotationMatrix::MakeFromX(Direction).Rotator();
	FRotator CurrentRotation = EnemyCharacter->GetActorRotation();
	float InterpSpeed = 7.f; 
	FRotator NewRotation = FMath::RInterpTo(CurrentRotation, TargetRotation, DeltaSeconds, InterpSpeed);
	NewRotation.Yaw += .525f;
	EnemyCharacter->SetActorRotation(NewRotation);
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
}
