#include "Tasks/AttackTask.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Math/Vector.h"
#include "Math/Rotator.h"
#include "Characters/CharacterTypes.h"
#include "Characters/Enemy/BasicEnemy.h"
#include "Kismet/KismetMathLibrary.h"

EBTNodeResult::Type UAttackTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* AIController = OwnerComp.GetAIOwner();
	if (!AIController) { return EBTNodeResult::Failed;}
	
	ABasicEnemy* EnemyCharacter = Cast<ABasicEnemy>(AIController->GetPawn());
	if (!EnemyCharacter) { return EBTNodeResult::Failed;}
	
	// Get Hostile Object from Blackboard
	UObject* HostileObject = OwnerComp.GetBlackboardComponent()->GetValueAsObject("Hostile");
	AActor* HostileActor = Cast<AActor>(HostileObject);
	if (!HostileActor) return EBTNodeResult::Failed;

	
	//EnemyCharacter->SetEnemyState(EEnemyState::EES_Attack);

	const FVector AttackLocation = HostileActor->GetActorLocation();
	EnemyCharacter->Attack(AttackLocation);
	
	return Super::ExecuteTask(OwnerComp, NodeMemory);
}
