#include "UI/HealthWidget.h"

#include "Components/Image.h"
void UHealthWidget::UpdateHealthDisplay(float CurrentHealth) const
{
	CurrentHealth = CurrentHealth * 100;
	if (HealthFull)
	{
		HealthFull->SetVisibility(CurrentHealth >= 60 ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
	if (HealthMid)
	{
		HealthMid->SetVisibility(CurrentHealth >= 30 && CurrentHealth < 60 ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
	if (HealthLow)
	{
		HealthLow->SetVisibility(CurrentHealth < 30 ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
}

void UHealthWidget::CollapseAll()
{
	if (HealthFull)
	{
		HealthFull->SetVisibility(ESlateVisibility::Collapsed);
	}
	if (HealthMid)
	{
		HealthMid->SetVisibility(ESlateVisibility::Collapsed);
	}
	if (HealthLow)
	{
		HealthLow->SetVisibility(ESlateVisibility::Collapsed);
	}
}
