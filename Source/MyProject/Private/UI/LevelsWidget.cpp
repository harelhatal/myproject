#include "UI/LevelsWidget.h"

#include "Components/TextBlock.h"

void ULevelsWidget::SetLevelTextBlock(FString LevelNumber) const
{
	const FText PreText = FText::FromString("LEVEL ");
	const FText LevelNumberText = FText::FromString(LevelNumber);
	const FText FullText = FText::Format(FText::FromString(TEXT("{0}{1}")), PreText, LevelNumberText);

	if (LevelTextBlock)
	{
		LevelTextBlock->SetText(FullText);
	}
}
void ULevelsWidget::SetBulletTextBlock(int BulletsLeft)
{
	const FText BulletsLeftText = FText::AsNumber(BulletsLeft);
	const FText PreText = FText::FromString("Bullets Left:");
	const FText FullText = FText::Format(FText::FromString(TEXT("{0}{1}")), PreText, BulletsLeftText);
	
	if (BulletsTextBlock)
	{
		BulletsTextBlock->SetText(FullText);
	}
}
