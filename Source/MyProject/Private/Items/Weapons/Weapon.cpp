﻿#include "Items/Weapons/Weapon.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Characters/CharacterTypes.h"
#include "Characters/FPCharacter.h"
#include "Components/SphereComponent.h"
#include "Components/TimelineComponent.h"
#include "Animation/AnimInstance.h"
#include "Animations/CustomAnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Items/Weapons/M4/Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

class UEnhancedInputLocalPlayerSubsystem;

AWeapon::AWeapon()
{
	if (ItemMesh)
	{
		ProjectileSpawnPos = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileSpawnPos"));
		ProjectileSpawnPos->SetupAttachment(ItemMesh);
		ProjectileSpawnPos->SetMobility(EComponentMobility::Movable);

		CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ADSCamera"));
		CameraComponent->SetupAttachment(RootComponent);
	}
	
	PeekingTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("PeekingTimeline"));
	RecoilTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("RecoilTimeline"));
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	if (PeekingCurve)
	{
		FOnTimelineFloat TimelineProgress;
		TimelineProgress.BindUFunction(this, FName("PeekingTimelineProgress"));
		PeekingTimeline->AddInterpFloat(PeekingCurve, TimelineProgress);
	}
	if (const APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0); PlayerController != nullptr)
	{
		StartControlRoll = PlayerController->GetControlRotation().Roll;
	}
	
	if (RecoilCurve)
	{
		FOnTimelineFloat TimelineProgress;
		TimelineProgress.BindUFunction(this, FName("RecoilTimelineProgress"));
		RecoilTimeline->AddInterpFloat(RecoilCurve, TimelineProgress);
	}
	OriginalRecoilItemMeshLocation = ItemMesh->GetRelativeLocation();
}

void AWeapon::Equip(AFPCharacter* InParent, const FName InSocketName) 
{
	WeaponOwner = InParent;
	AttachMeshToSocket(WeaponOwner->GetMesh(), InSocketName);

	if (const APlayerController* PlayerController = Cast<APlayerController>(InParent->GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(WeaponMappingContext, 1);
		}

		if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController->InputComponent))
		{
			EnhancedInputComponent->BindAction(AimHoldAction, ETriggerEvent::Triggered, this, &AWeapon::AimHoldTriggered);
			EnhancedInputComponent->BindAction(AimHoldAction, ETriggerEvent::Completed, this, &AWeapon::AimHoldReleased);
			EnhancedInputComponent->BindAction(AdsAction, ETriggerEvent::Triggered, this, &AWeapon::AdsTriggered);
			EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Started, this, &AWeapon::FirePressed);
			EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Completed, this, &AWeapon::FireReleased);
			EnhancedInputComponent->BindAction(PeekAction, ETriggerEvent::Triggered, this, &AWeapon::PeekHandler);
			EnhancedInputComponent->BindAction(BreathAction, ETriggerEvent::Triggered, this, &AWeapon::BreathTriggered);
			EnhancedInputComponent->BindAction(BreathAction, ETriggerEvent::Completed, this, &AWeapon::BreathCompleted);

		}
	}
}
	
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeapon::DisableSphereCollision() const
{
	if (Sphere)
	{
		Sphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void AWeapon::AttachMeshToSocket(USceneComponent* InParent, const FName& InSocketName) const
{
	const FAttachmentTransformRules TransformRules(EAttachmentRule::SnapToTarget, true);
	ItemMesh->AttachToComponent(InParent, TransformRules, InSocketName);
}

void AWeapon::AttackEnemy(FVector AttackLocation)
{
	UWorld* World = GetWorld();
	if (!World) { return ;}

	const FVector SpawnLocation = ProjectileSpawnPos->GetComponentLocation();
	FRotator SpawnRotation = ProjectileSpawnPos->GetForwardVector().Rotation();;
	
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	//AProjectile* Projectile = World->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, SpawnParams);

	// Muzzle effect
	FVector MuzzleSpawnLocation = ProjectileSpawnPos->GetSocketLocation(TEXT("Muzzle"));
	FRotator MuzzleSpawnRotation = ProjectileSpawnPos->GetSocketRotation(TEXT("Muzzle"));
	if (World)
	{
		UParticleSystemComponent* MuzzleEffectComponent = UGameplayStatics::SpawnEmitterAtLocation(
			World,
			MuzzleEffect,
			MuzzleSpawnLocation,
			MuzzleSpawnRotation,
			FVector(0.1f, 0.f, 0.f),
			true
		);
		// FTimerHandle UnusedHandle;
		// TWeakObjectPtr<UParticleSystemComponent> WeakMuzzleEffectComponent = MuzzleEffectComponent;
		//
		// // World->GetTimerManager().SetTimer(UnusedHandle, [WeakMuzzleEffectComponent]()
		// // {
		// // 	if (WeakMuzzleEffectComponent.IsValid())
		// // 	{
		// // 		WeakMuzzleEffectComponent->DestroyComponent();
		// // 	}
		// // }, 0.1f, false);
	}


	FVector EndLocation = SpawnLocation + (SpawnRotation.Vector() * 20000);
	EndLocation.Z = AttackLocation.Z;

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	//QueryParams.AddIgnoredActor(Projectile); 
	QueryParams.bTraceComplex = true;

	if (bool bHit = World->LineTraceSingleByChannel(HitResult, SpawnLocation, EndLocation, ECC_Visibility, QueryParams))
	{
		if (GEngine && HitResult.GetActor())
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Hit: %s"), *HitResult.GetActor()->GetName()));
		}
		if (IHitInterface* HitInterface = Cast<IHitInterface>(HitResult.GetActor()))
		{
			HitInterface->ReceiveDamage(20);
		}
		
	}
	DrawDebugLine(
	World,
	SpawnLocation,
	EndLocation,
	FColor::Green, // Color of the line
	false, // Persistent (remains for more than one frame)
	5.0f, // Lifetime (in seconds)
	0, // Depth priority
	1.0f // Thickness
	);

}

void AWeapon::AimHoldTriggered()
{
	if (WeaponOwner->GetCameraState() != ECameraState::ECS_Weapon)
	{
		WeaponOwner->SetCharacterState(ECharacterState::ECS_HoldAim);
		WeaponOwner->ShowHoldCrosshair();
	}
}


void AWeapon::AimHoldReleased()
{
	if (WeaponOwner->GetCharacterState()  == ECharacterState::ECS_HoldAim)
	{
		WeaponOwner->HideHoldCrosshair();
		WeaponOwner->SetCharacterState(ECharacterState::ECS_Equipped);
	}
}

void AWeapon::BreathCompleted()
{
	if (WeaponOwner->GetBreathState() == EBreathState::EBS_Stop)
	{
		WeaponOwner->SetBreathState(EBreathState::EBS_Breath);
	}
}

void AWeapon::BreathTriggered()
{
	if (WeaponOwner->CameraState == ECameraState::ECS_Weapon)
	{
		if (WeaponOwner->GetBreathState() == EBreathState::EBS_Breath)
		{
			WeaponOwner->SetBreathState(EBreathState::EBS_Stop);
		}
	}
}

void AWeapon::AdsTriggered()
{
	if (WeaponOwner->GetCharacterState() == ECharacterState::ECS_Equipped)
	{
		WeaponOwner->SetCharacterState(ECharacterState::ECS_AdsAim);
		EnableAds();
	} else if (WeaponOwner->GetCharacterState()  == ECharacterState::ECS_AdsAim)
	{
		WeaponOwner->SetCharacterState(ECharacterState::ECS_Equipped);
		DisableAds();
		WeaponOwner->SetBreathState(EBreathState::EBS_Breath);
	}
}

void AWeapon::EnableAds()
{
	OriginalRotate = WeaponOwner->GetMesh()->GetRelativeRotation();
	WeaponOwner->GetMesh()->SetRelativeRotation(FRotator(OriginalRotate.Pitch,OriginalRotate.Yaw,18.5f));
	WeaponOwner->SetNewViewTarget(this);
	WeaponOwner->SetCameraState(ECameraState::ECS_Weapon);
}

void AWeapon::DisableAds()
{
	WeaponOwner->GetMesh()->SetRelativeRotation(OriginalRotate);
	WeaponOwner->SetNewViewTarget(WeaponOwner);
	WeaponOwner->SetCameraState(ECameraState::ECS_Mesh);
}

void AWeapon::FirePressed()
{
	Shoot();
	
	// Count projectiles
	CountShots ++;
	
	WeaponOwner->OnShot(CountShots);
}

void AWeapon::FireReleased()
{
	WeaponOwner->PlayAttackMontage();
}

void AWeapon::PeekingTimelineProgress(float Value) const
{
	if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0))
	{
		
		const FRotator CurrentControlRotation = PlayerController->GetControlRotation();
		const float NewRoll = FMath::Lerp(StartControlRoll, PeekRotationRate, Value);
		const FRotator NewRotation = FRotator(CurrentControlRotation.Pitch, CurrentControlRotation.Yaw, NewRoll);
		
		PlayerController->SetControlRotation(NewRotation);
	}
}

void AWeapon::RecoilTimelineProgress(float Value) const
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (PlayerController == nullptr) { return ;}
	
	const FRotator CurrentControlRotation = PlayerController->GetControlRotation();
	const float NewPitch = FMath::Lerp(StartRecoilPitch, StartRecoilPitch + EndRecoilPitch, Value);
	const FRotator NewRotation = FRotator(NewPitch, CurrentControlRotation.Yaw, CurrentControlRotation.Roll);
	
	//const FVector NewLocation = FMath::Lerp(StartRecoilLocation, StartRecoilLocation + EndRecoilLocation, Value);

	PlayerController->SetControlRotation(NewRotation);
	
	if (WeaponOwner->GetCameraState() == ECameraState::ECS_Mesh)
	{
		//WeaponOwner->CameraView->SetRelativeLocation(NewLocation);
	} else if (WeaponOwner->GetCameraState() == ECameraState::ECS_Weapon)
	{
		//ItemMesh->SetRelativeLocation(NewLocation);
	}
}

void AWeapon::RecoilTimelineFinished()
{
	ItemMesh->SetRelativeLocation(OriginalRecoilItemMeshLocation);
}

void AWeapon::ProceduralRecoil()
{
	FVector RandomLocation = FVector(
		FMath::RandRange(1.f, 2.5f), // X
		0.f, // Y
		0.f // Z
	);
	
	EndRecoilPitch = FMath::RandRange(3.f, 7.f);
	EndRecoilLocation = RandomLocation;
}

void AWeapon::PeekHandler(const FInputActionValue& Value)
{
	const float FloatValue = Value.Get<float>();
	if (WeaponOwner->GetPeekingState() == EPeekingState::EPS_NotPeeking)
	{
		WeaponOwner->SetPeekingState(FloatValue == 1 ? EPeekingState::EPS_PeekingRight: EPeekingState::EPS_PeekingLeft);
		PeekRotationRate = FloatValue == 1 ? 20.f : -20.f;
		PeekingTimeline->Play();
	} else
	{
		WeaponOwner->SetPeekingState(EPeekingState::EPS_NotPeeking);
		PeekingTimeline->Reverse();
	}
}

void AWeapon::Shoot()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	APlayerCameraManager* CameraManager = PlayerController->PlayerCameraManager;
	UWorld* World = GetWorld();
	UCustomAnimInstance* CustomAnimInstance = Cast<UCustomAnimInstance>(WeaponOwner->GetMesh()->GetAnimInstance());

	if (!ProjectileClass || PlayerController == nullptr || CameraManager == nullptr || !World || CustomAnimInstance == nullptr)
	{
		return ;
	}
	
	// Muzzle
	/*FVector MuzzleSpawnLocation = ProjectileSpawnPos->GetComponentLocation() + (ProjectileSpawnPos->GetForwardVector() * 10.0f);
	UParticleSystemComponent* MuzzleEffectComponent = UGameplayStatics::SpawnEmitterAtLocation(
	GetWorld(),
	MuzzleEffect,
	MuzzleSpawnLocation,
	ProjectileSpawnPos->GetComponentRotation(),
	FVector(0.06f, 0.f, 0.f),
	true 
	);
	
	FTimerHandle UnusedHandle;
	TWeakObjectPtr<UParticleSystemComponent> WeakMuzzleEffectComponent = MuzzleEffectComponent;
	
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, [WeakMuzzleEffectComponent]()
	{
		if (WeakMuzzleEffectComponent.IsValid())
		{
			WeakMuzzleEffectComponent->DestroyComponent();
		}
	}, 0.1f, false);*/
	
	// FIRE LINE TRACE
	
	const FVector StartLocation = CameraManager->GetCameraLocation();
	FRotator LineRotation = CameraManager->GetCameraRotation();
	FVector EndLocation = StartLocation + (LineRotation.Vector() * 20000);

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(WeaponOwner); 
	QueryParams.bTraceComplex = true;

	if (bool bHit = World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, QueryParams))
	{
		if (IHitInterface* HitInterface = Cast<IHitInterface>(HitResult.GetActor()))
		{
			HitInterface->ReceiveDamage(100);
		}
		FVector Location = HitResult.ImpactPoint; 
		FRotator Rotation = HitResult.ImpactNormal.Rotation();

		if (HitEffect && World)
		{
			UParticleSystemComponent* SpawnedEffect = UGameplayStatics::SpawnEmitterAtLocation(World, HitEffect, Location, Rotation, FVector(0.1f, 0.1f, 0.1f), true, EPSCPoolMethod::AutoRelease);
			// if (SpawnedEffect)
			// {
			// 	FTimerHandle TimerHandleHitEffect;
			// 	World->GetTimerManager().SetTimer(TimerHandleHitEffect, [SpawnedEffect]()
			// 	{
			// 		SpawnedEffect->DeactivateSystem();
			// 	}, 0.3f, false);
			// }
		}
	}
	
	// Recoil
	if (WeaponOwner->GetCameraState() == ECameraState::ECS_Mesh)
	{
		StartRecoilLocation = WeaponOwner->CameraView->GetRelativeLocation();
	} else if (WeaponOwner->GetCameraState() == ECameraState::ECS_Weapon)
	{
		StartRecoilLocation = ItemMesh->GetRelativeLocation();
	}
	StartRecoilPitch = PlayerController->GetControlRotation().Pitch;
	ProceduralRecoil();
	RecoilTimeline->PlayFromStart();
	if (World)
	{
		World->GetTimerManager().SetTimer(TimerHandle, this, &AWeapon::RecoilTimelineFinished, 0.2f, false);
	}
}
