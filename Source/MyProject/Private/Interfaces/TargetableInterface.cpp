﻿#include "Interfaces/TargetableInterface.h"


void ITargetableInterface::OnTargetDestroyedHandler()
{
	OnTargetDestroyed.Broadcast();
}