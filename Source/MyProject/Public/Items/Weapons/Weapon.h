﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Items/Item.h"
#include "Weapon.generated.h"

class UCameraComponent;
class UTimelineComponent;
class APlayableCharacter;
class AFPCharacter;
class UInputAction;
class UInputMappingContext;
class UArrowComponent;
class USoundBase;
class UBoxComponent;

UCLASS()
class MYPROJECT_API AWeapon : public AItem
{
	GENERATED_BODY()
public:
	AWeapon();

	AFPCharacter* WeaponOwner;
	
	UPROPERTY(EditAnywhere, Category=Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;
	
	UPROPERTY(EditAnywhere)
	UArrowComponent* ProjectileSpawnPos;
	
	void Equip(AFPCharacter* InParent, const FName InSocketName);
	void AttachMeshToSocket(USceneComponent* InParent, const FName& InSocketName) const;
	void AttackEnemy(FVector AttackLocation);

	// Count projectiles
	float CountShots = 0;
	
protected:
	virtual void Tick(float DeltaTime) override;
	
	virtual void BeginPlay() override;
	
	void DisableSphereCollision() const;
	
	// Mapping context
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputMappingContext* WeaponMappingContext;

	// Aim with weapon
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input) UInputAction* AimHoldAction;
	
	virtual void AimHoldTriggered();
	virtual void AimHoldReleased();
	
	// Breath state
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input) UInputAction* BreathAction;
	virtual void BreathCompleted();
	virtual void BreathTriggered();

	// Aim Down Sight
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input) UInputAction* AdsAction;
	
	virtual void AdsTriggered();
	virtual void EnableAds();
	virtual void DisableAds();

	
	// Fire with weapon
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input) UInputAction* FireAction;
	UFUNCTION(BlueprintSetter, Category="Fire") virtual void FirePressed();
	virtual void FireReleased();
	virtual void Shoot();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	// Peak
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input) UInputAction* PeekAction;
	virtual void PeekHandler(const FInputActionValue& Value);
	float StartControlRoll;
	float PeekRotationRate;

	// muzzle effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* MuzzleEffect;

	// Hit effect
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* HitEffect;



private:
	FRotator OriginalRotate;

	// Peek timeline
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UTimelineComponent* PeekingTimeline;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	UCurveFloat* PeekingCurve;

	UFUNCTION()
	void PeekingTimelineProgress(float Value) const;

	// Recoil timeline
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UTimelineComponent* RecoilTimeline;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	UCurveFloat* RecoilCurve;

	UFUNCTION()
	void RecoilTimelineProgress(float Value) const;
	UFUNCTION()
	void RecoilTimelineFinished();
	float StartRecoilPitch;
	float EndRecoilPitch;
	FVector StartRecoilLocation;
	FVector EndRecoilLocation;

	void ProceduralRecoil();
	FTimerHandle TimerHandle;

	FVector OriginalRecoilItemMeshLocation;
};
