// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "Items/Weapons/Weapon.h"
#include "M4.generated.h"

class UCameraComponent;
/**
 * 
 */
UCLASS()
class MYPROJECT_API AM4 : public AWeapon
{
	GENERATED_BODY()
public:
	AM4();

protected:
};