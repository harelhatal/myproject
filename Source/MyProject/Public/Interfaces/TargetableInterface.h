﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TargetableInterface.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTargetDestroyed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTargetCreated);

UINTERFACE(MinimalAPI)
class UTargetableInterface : public UInterface
{
	GENERATED_BODY()
};

class MYPROJECT_API ITargetableInterface
{
	GENERATED_BODY()

public:

	FOnTargetDestroyed OnTargetDestroyed;
	FOnTargetCreated OnTargetCreated;
	void OnTargetDestroyedHandler();
	
};
