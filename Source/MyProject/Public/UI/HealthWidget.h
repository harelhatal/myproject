#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HealthWidget.generated.h"

class UImage;

UCLASS()
class MYPROJECT_API UHealthWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	float Health;
	
	UPROPERTY(meta = (BindWidget))
	UImage* HealthFull;

	UPROPERTY(meta = (BindWidget))
	UImage* HealthMid;

	UPROPERTY(meta = (BindWidget))
	UImage* HealthLow;
	
	UFUNCTION(BlueprintCallable, Category = "HUD")
	void UpdateHealthDisplay(float CurrentHealth) const;

	void CollapseAll();
};
