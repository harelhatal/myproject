// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LevelsWidget.generated.h"


class UTextBlock;

UCLASS()
class MYPROJECT_API ULevelsWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetLevelTextBlock(FString LevelNumber) const;
	void SetBulletTextBlock(int BulletsLeft);

	UPROPERTY(meta = (BindWidget))
	UTextBlock* LevelTextBlock;
	UPROPERTY(meta = (BindWidget))
	UTextBlock* BulletsTextBlock;
protected:
	
	
};
