// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CustomGamemode.generated.h"


UCLASS()
class MYPROJECT_API ACustomGamemode : public AGameModeBase
{
	GENERATED_BODY()
public:
	void OnLevelSucceeded();
	void CheckShotCount(int ShotCount);
	virtual void BeginPlay() override;
	FName CurrentLevelName;
	int GetCurrentLevelMaxShot();
	UFUNCTION()
	void RestartLevel();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maps")
	TMap<FName, FName> NextLevelMap;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maps")
	TMap<FName, int> MaxShotLevelMap;
	UFUNCTION()
	void OnTargetDestroyed();
	
	int TargetAmount;
	void InitializeLevel();

	void OpenLevel(FName LevelName);

};
