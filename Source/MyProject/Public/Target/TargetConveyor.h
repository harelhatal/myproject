#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TargetConveyor.generated.h"

UCLASS()
class MYPROJECT_API ATargetConveyor : public AActor
{
	GENERATED_BODY()
	
public:	
	ATargetConveyor();
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	float NumberOfTargets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	float DistanceBetweenTargets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	float MovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	float AddedDistance;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	bool Left;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Targets")
	TSubclassOf<class ATarget> TargetClass;

	FVector StartLocation;
	FVector EndLocation;

private:
	TArray<ATarget*> Targets;

	void InitializeTargets();
	void MoveTargets(float DeltaTime);

};
