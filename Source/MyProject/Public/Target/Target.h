// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/HitInterface.h"
#include "Interfaces/TargetableInterface.h"
#include "Target.generated.h"

class UProjectileMovementComponent;
class UAttributesComponent;

UCLASS()
class MYPROJECT_API ATarget : public AActor, public IHitInterface, public ITargetableInterface
{
	GENERATED_BODY()
	
public:	
	ATarget();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Attributes")
	UAttributesComponent* Attributes;
	
	virtual void Tick(float DeltaTime) override;
	virtual void ReceiveDamage(float Damage) override;

	bool IsAlive() const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* ItemMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effect")
	UParticleSystem* DieEffect;

private:
	void CheckHealthAndAct();
};
