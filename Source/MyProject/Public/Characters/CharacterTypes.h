﻿#pragma once

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	ECS_Equipped UMETA(DisplayName = "Equipped"),
	ECS_Fire UMETA(DisplayName = "Fire"),
	ECS_HoldAim UMETA(DisplayName = "Hold Aim"),
	ECS_AdsAim UMETA(DisplayName = "Aim Down Sight"),
};

UENUM(BlueprintType)
enum class EPeekingState : uint8
{
	EPS_NotPeeking UMETA(DisplayeName = "Not Peeking"),
	EPS_PeekingRight UMETA(DisplayName = "Peeking Right"),
	EPS_PeekingLeft UMETA(DisplayName = "Peeking Left")
};

UENUM(BlueprintType)
enum class ECameraState : uint8
{
	ECS_Mesh UMETA(DisplayeName = "Mesh Camera"),
	ECS_Weapon UMETA(DisplayName = "Weapon Camera"),
};

UENUM(BlueprintType)
enum class EBreathState : uint8
{
	EBS_Breath UMETA(DisplayeName = "Breathing"),
	EBS_Stop UMETA(DisplayName = "Stopped Breathing"),
};

UENUM(BlueprintType)
enum class EEnemyState : uint8
{
	EES_Idle UMETA(DisplayeName = "Idle"),
	EES_WaitToAttack UMETA(DisplayName = "Waiting to attack"),
	EES_Attack UMETA(DisplayName = "Attacking")
};