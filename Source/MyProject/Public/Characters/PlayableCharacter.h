#pragma once

#include "CoreMinimal.h"
#include "Characters/BaseCharacter.h"
#include "Interfaces/HitInterface.h"
#include "PlayableCharacter.generated.h"

class AWeapon;
struct FInputActionValue;
class UInputAction;
class UInputMappingContext;
class USpringArmComponent;
class UCameraComponent;

UCLASS()
class MYPROJECT_API APlayableCharacter : public ABaseCharacter, public IHitInterface
{
	GENERATED_BODY()

public:
	APlayableCharacter();
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMeshComponent* FullBodyMesh;

	// Inputs
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// Character State (Get Set)
	FORCEINLINE void SetCharacterState(const ECharacterState NewCharacterState) { CharacterState = NewCharacterState; }
	FORCEINLINE ECharacterState GetCharacterState() const { return CharacterState;}

	// HIT interface
	virtual void PlayAttackMontage() const override;

	// Hold item/weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	TSubclassOf<AWeapon> WeaponClass;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* CameraView;

	//Character attack state
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	ECharacterState CharacterState;



protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputMappingContext* MappingContext;
	
	// Move player
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* MoveAction;
	virtual void Walk(const FInputActionValue& Value);
	float WalkingSpeed = 1.f;
	float WalkingSpeedHoldAim = .65f;

	// Move camera action
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* MoveCameraAction;
	virtual void MoveCamera(const FInputActionValue& Value);
	virtual void ReceiveDamage(float Damage) override;
	float CameraRotationSpeed;
	
	// Jump
	FORCEINLINE virtual void JumpStart() { bPressedJump = true;}
	FORCEINLINE virtual void JumpStop() { bPressedJump = false;}
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* JumpAction;

};
