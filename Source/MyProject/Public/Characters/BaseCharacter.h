// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

enum class ECharacterState : uint8;
class UAttributesComponent;

UCLASS()
class MYPROJECT_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:	ABaseCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void PlayHitReactMontage(const FName& SectionName);
	virtual void PlayAttackMontage() const;
	virtual void PlayDeathMontage();

protected:
	virtual void BeginPlay() override;
	
	// Montages
	void PlayMontageSection(UAnimMontage* Montage, const FName& SectionName) const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAttributesComponent* Attributes;

	
	UPROPERTY(EditDefaultsOnly, Category = Combat)
	UAnimMontage* AttackMontage;

	UPROPERTY(EditDefaultsOnly, Category = Combat)
	UAnimMontage* DeathMontage;

	UPROPERTY(EditDefaultsOnly, Category = Combat)
	UAnimMontage* HitReactMontage;
};
