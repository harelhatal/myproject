#pragma once

#include "CoreMinimal.h"
#include "Characters/BaseCharacter.h"
#include "Interfaces/HitInterface.h"
#include "BasicEnemy.generated.h"

class AWeapon;
class UAIBehavior;
enum class EEnemyState : uint8;

UCLASS()
class MYPROJECT_API ABasicEnemy : public ABaseCharacter, public IHitInterface
{
	GENERATED_BODY()
public:
	ABasicEnemy();
	FORCEINLINE UAIBehavior* GetAIBehavior() const { return Behavior; }

	// Enemy State(Set Get)
	FORCEINLINE void SetEnemyState(const EEnemyState NewState) {EnemyState = NewState;}
	FORCEINLINE EEnemyState GetEnemyState() const { return EnemyState;}
	virtual void Tick(float DeltaSeconds) override;
	void Attack(FVector AttackLocation) const;
	void PlayAttackMontage() const override;
	void ChangeRotation(FRotator NewRotation) const;
protected:
	virtual void BeginPlay() override;
	EEnemyState EnemyState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI Behavior")
	UAIBehavior* Behavior;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon")
	TSubclassOf<AWeapon> WeaponClass;

	AWeapon* WeaponSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Killable")
	bool IsKillable;
	virtual void ReceiveDamage(float Damage) override;
};
