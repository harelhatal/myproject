#pragma once

#include "CoreMinimal.h"
#include "Characters/PlayableCharacter.h"
#include "FPCharacter.generated.h"
enum class EBreathState : uint8;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerDies);

class ACustomGamemode;
class ABaseLevel;
class ULevelsWidget;
class UHealthWidget;
enum class ECameraState : uint8;
enum class EPeekingState : uint8;
class UTimelineComponent;

UCLASS()
class MYPROJECT_API AFPCharacter : public APlayableCharacter
{
	GENERATED_BODY()
public:
	
	AFPCharacter();

	// Peeking state
	EPeekingState PeekingState;
	FORCEINLINE void SetPeekingState(EPeekingState NewState) { PeekingState = NewState;}
	FORCEINLINE EPeekingState GetPeekingState() { return PeekingState;}

	// Camera state
	ECameraState CameraState;
	FORCEINLINE void SetCameraState(ECameraState NewState) { CameraState = NewState;}
	FORCEINLINE ECameraState GetCameraState() const { return CameraState;}

	// Camera state
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EBreathState BreathState;
	FORCEINLINE void SetBreathState(EBreathState NewState) { BreathState = NewState;}
	FORCEINLINE EBreathState GetBreathState() const { return BreathState;}

	void ShowHoldCrosshair();
    void HideHoldCrosshair();

    // Crosshair
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> HoldCrosshairWidgetClass;
	UUserWidget* HoldCrosshairWidget;
	
    UFUNCTION()
	float GetRightLeftAngle();

	// Switch View Target
	void SetNewViewTarget(AActor* NewTarget);

	// Shooting trigger
	float CountShot;
	void OnShot(float NewCountShot);

	// HUD
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UHealthWidget> HealthWidgetClass;
	UHealthWidget* HealthWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<ULevelsWidget> LevelsWidgetClass;
	ULevelsWidget* LevelsWidget;

	// Die Delegate
	FOnPlayerDies OnPlayerDies;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Monatge")
	UAnimMontage* AimMontage;
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void ReceiveDamage(float Damage) override;
	virtual void Walk(const FInputActionValue& Value) override;

private:
	ABaseLevel* GetCurrentLevelInstance();
	ACustomGamemode* Gamemode;


};
