// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CustomAnimInstance.generated.h"

enum class EBreathState : uint8;
class AFPCharacter;
enum class ECharacterState : uint8;

UCLASS()
class MYPROJECT_API UCustomAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	
	FORCEINLINE void SetCharacterState(ECharacterState NewState) { CharacterState = NewState; }
	FORCEINLINE void SetSpeed(float CurrentSpeed) { Speed = CurrentSpeed; }
	FORCEINLINE bool GetIsJumping() const {return IsJumping;}
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	ECharacterState CharacterState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	EBreathState BreathState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsJumping;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float RightLeftAngle;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	
	AFPCharacter* OwningCharacter;

private:
};
