// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "EnemyAnimInstance.generated.h"

class ABasicEnemy;
enum class EEnemyState : uint8;

UCLASS()
class MYPROJECT_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	FORCEINLINE void SetEnemyState(const EEnemyState NewState) { EnemyState = NewState; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State")
	EEnemyState EnemyState;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	ABasicEnemy* OwningEnemy;
};
